%title: Docker Training 2019
%author: Ian Merrick - Biocomputing hub 
%date: 2019-08-07

-> Schedule <-
==============

-> Session 1 <-
-> --------- <-
 1. Class Environment Setup
 2. containers from first principles
 3. docker images, containers and repositories
 4. running your first containers
 5. try not to do anything silly!
 6. more advanced docker
 7. building images
 
-> Session 2 <-
-> --------- <-
 8. guided build of your own images

-------------------------------------------------

-> 1. Class Environment Setup <-
=============================


A shared linux server is available for you to run your own docker commands

You will be running the docker commands under your own user account on this server.

This server will exist only for the duration of this training course.

```
ssh [username]@[host]
```

...or use an ssh client like *bitav*

-------------------------------------------------


-> 2. Grokking Containers <-
============================

What/why containers?
 * Containers are virtual environments
 * Similar to Virtual Machines (VMs), except containers virtualize the kernel; VMs virtualize hardware.
 * Faster than a VM
 * Portable
 * Lightweight
 * Repeatable

What about docker?
 * Docker is an implementation of Containers (there are others)
 * Docker is the most widespread container technology because of its ease of use (in my opinion)
 * Docker images can be stored in docker registries (accessible over http/s)
   * https://hub.docker.com
   * https://quay.io
   * https://bigr.bios.cf.ac.uk
   * .. plus many more
 * Use Docker Engine (installed on your computer) to pull (download) images and run them as containers.


-------------------------------------------------

-> 3. docker images, containers and repositories <-
================================================

 * images can be layered
 * containers can be namespaced
 * containers can have resorce limits imposed (cgroups)
 * public repositories exist

-------------------------------------------------

-> 4. running your first container <- 
==================================

 * pull
 * run inside an interactive shell
 * exiting a container (p's and q's)
 * mount local folders
 * run without a shell


-------------------------------------------------


-> 5. try not to do anything silly! <-
===================

Don't try this at home.




-------------------------------------------------

-> 6. more advanced docker <-
=============================

 * linking containers - with 'link'  (old method)
 * linking containers - with 'networks' (current method)
 * opening ports
 * graphics

-------------------------------------------------

-> 7. building images <-
========================



-------------------------------------------------

-> presentation tools <-
========================

 * tmux
 * mdp
 * http://asciiflow.com/

-------------------------------------------------

-> 8. guided build of your own images <-
========================================


