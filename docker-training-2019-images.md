%title: Docker Training 2019
%author: Ian Merrick - Biocomputing hub 
%date: 2019-08-07

-> Containers vs. VMs <-
========================


.                          +---------------------+
.                          | apps/libs|          |
.                          |          | apps/libs|
.                          +---------------------+
.                          | VM1      | VM2      |
.                          | kernel/OS| kernel/OS|
. +------------------+     +---------------------+
. |Containers....|Docker   |                     |
. |  |  |  |  |  |Engine   | HyperVisor          |
. +--+--+--+--+------+     +---------------------+
. | Host             |     | Host                |
. | kernel / OS      |     | kernel / OS         |
. +------------------+     +---------------------+
. |                  |     |                     |
. |hardware          |     | hardware            |
. |                  |     |                     |
. +------------------+     +---------------------+
.
.    Containers              Virtual Machines


-------------------------------------------------



